@extends('app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><div style="text-align: center;">Hola {{ $moneda->currency}}</div></div>

                <div class="panel-body" style="background: #b7babf;border: 1px solid;">
                  <div class="col-md-6">
                  	<form action="">
					<input id="id" name="id" type="hidden" value="{{$moneda->idcurrency}}">

					<div class="layout-row min-size">
						<div class="form-tabless-fields ">
							<div class="form-group text-field is-required">
								<label for="">Name</label>
								<input
									type="text"
									name="name"
									id="name"
									value=""
									placeholder=""
									class="form-control"
									autocomplete="off"
									maxlength="255"/>
							</div>
							<div class="form-group text-field is-required">
								<label for="">Username</label>
								<input
									type="text"
									name="username"
									id="username"
									value=""
									placeholder=""
									class="form-control"
									autocomplete="off"
									maxlength="255"/>
							</div>	
							<div class="form-group text-field is-required">
								<label for="">Email</label>
								<input
									type="email"
									name="email"
									id="email"
									value=""
									placeholder=""
									class="form-control"
									autocomplete="off"
									maxlength="255"/>
							</div>
							<div class="form-group text-field">
								<label for="">Password</label>
								<input
							        type="password"
							        name="password"
							        id="password"
							        value=""
							        class="form-control"
							        autocomplete="off"
							        maxlength="255"/>
							</div>
							<div class="form-group text-field">
								<label for="">Password Confirmation</label>
								<input
							        type="password"
							        name="password_confirm"
							        id="password_confirm"
							        value=""
							        class="form-control"
							        autocomplete="off"
							        maxlength="255"/>
							</div>
							<div class="form-group text-field span-left">
								<div class="form-buttons">
						            <button
					                    type="submit"
					                    data-hotkey="ctrl+s, cmd+s"
					                    data-load-indicator="Editing User..."
					                    class="btn btn-primary form-control">
					                    Edit</button>
						        </div> 
							</div>		
						</div>	
					</div>	
				</form>
                  </div>
                  <div class="col-md-6">
                  	
                  </div>                  
                </div>                         
            </div>
        </div>
    </div>
</div>
@stop