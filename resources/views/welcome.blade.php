<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Kraken Team</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #F1F8E0;
                font-family: 'Raleway', sans-serif;
                font-weight: 680;
                font-size: 20;
                margin: 0;
            }

            body{
                background-image: url("fullscreen.jpg");
                background-size: cover;
                background-repeat: no-repeat;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 20px;
                top: 20px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #F1F8E0;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .icon-image{
                height: 20%;
                width: 20%;
                float: right;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
              @if (Auth::guest())
                <a href="{{route('login')}}">Login</a>
                <a href="{{route('register')}}">Register</a>
              @else
                 <a href="{{ url('/') }}"><img class="icon-image" src="favicon.ico"></a>
              @endif
            </div>

            <div class="content">
                <div class="title m-b-md">
                    KЯAKΣИ
                </div>

                <div class="links">
                    <a href="https://towerhousestudio.com/" target="_blank" >Tower</a>
                    <a href="https://towerhousestudio.com/team/" target="_blank" >Team</a>
                    <a href="{{ url('/monedas') }}" >Backend</a>
                </div>
            </div>
        </div>
    </body>
</html>
