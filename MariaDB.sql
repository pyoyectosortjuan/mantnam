-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: MariaDB
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cb_address`
--

DROP TABLE IF EXISTS `cb_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_address` (
  `idaddress` int(11) NOT NULL AUTO_INCREMENT,
  `idaddresses` int(11) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `postalnumber` varchar(20) DEFAULT NULL,
  `mainphone` varchar(100) DEFAULT NULL,
  `movilephone` varchar(100) DEFAULT NULL,
  `phone2` varchar(100) DEFAULT NULL,
  `phone3` varchar(100) DEFAULT NULL,
  `carrier` varchar(200) DEFAULT NULL,
  `addresstype` varchar(100) DEFAULT NULL,
  `locality` varchar(250) DEFAULT NULL,
  `estate` varchar(250) DEFAULT NULL,
  `idcountry` int(11) DEFAULT NULL,
  `notes1` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idaddress`),
  KEY `fk_cb_address_idaddresses` (`idaddresses`),
  KEY `fk_cb_address_idcountry` (`idcountry`),
  CONSTRAINT `fk_cb_address_idaddresses` FOREIGN KEY (`idaddresses`) REFERENCES `cb_addresses` (`idaddresses`),
  CONSTRAINT `fk_cb_address_idcountry` FOREIGN KEY (`idcountry`) REFERENCES `cb_country` (`idcountry`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Direcciones para: Clientes, Empresas,... ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_address`
--

LOCK TABLES `cb_address` WRITE;
/*!40000 ALTER TABLE `cb_address` DISABLE KEYS */;
INSERT INTO `cb_address` VALUES (2,5,'Ruth Perez 34 1º E','31908','625 63 96 36','625 63 96 37','625 63 96 38','','SEUR','DIRECCIÓN DE ENTREGA','Madrid','MADRID',275,'Entrega de 9:00 a 14:00 de L a V');
/*!40000 ALTER TABLE `cb_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cb_addresses`
--

DROP TABLE IF EXISTS `cb_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_addresses` (
  `idaddresses` int(11) NOT NULL AUTO_INCREMENT,
  `addressesentity` varchar(100) NOT NULL,
  PRIMARY KEY (`idaddresses`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Agrupación de las direcciones asignadas a una entidad.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_addresses`
--

LOCK TABLES `cb_addresses` WRITE;
/*!40000 ALTER TABLE `cb_addresses` DISABLE KEYS */;
INSERT INTO `cb_addresses` VALUES (5,'');
/*!40000 ALTER TABLE `cb_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cb_country`
--

DROP TABLE IF EXISTS `cb_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_country` (
  `idcountry` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `countrycode` varchar(2) NOT NULL,
  `hasregion` varchar(1) NOT NULL DEFAULT 'N',
  `regionname` varchar(60) DEFAULT NULL,
  `expressionphone` varchar(20) DEFAULT NULL,
  `displaysequence` varchar(20) NOT NULL,
  `isdefault` varchar(1) NOT NULL DEFAULT 'N',
  `ibannodigits` decimal(10,0) DEFAULT NULL,
  `ibancountry` varchar(2) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `idlanguage` varchar(6) DEFAULT NULL,
  `idcurrency` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcountry`),
  UNIQUE KEY `un_cb_country_countrycode` (`countrycode`),
  KEY `fk_cb_country_idcurrency` (`idcurrency`),
  KEY `fk_cb_country_idlanguage` (`idlanguage`),
  CONSTRAINT `fk_cb_country_idcurrency` FOREIGN KEY (`idcurrency`) REFERENCES `cb_currency` (`idcurrency`),
  CONSTRAINT `fk_cb_country_idlanguage` FOREIGN KEY (`idlanguage`) REFERENCES `cb_language` (`idlanguage`)
) ENGINE=InnoDB AUTO_INCREMENT=510 DEFAULT CHARSET=latin1 COMMENT='Tabla donde se definen todos los países con sus características principales: idioma, nombre, ..., y diferentes datos íntrinsecos a cada país.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_country`
--

LOCK TABLES `cb_country` WRITE;
/*!40000 ALTER TABLE `cb_country` DISABLE KEYS */;
INSERT INTO `cb_country` VALUES (270,'United States','United States of America','US','Y','State','Y','@C@, @R@ @P@','Y',NULL,'',1,'en_US',7),(271,'Germany','Germany','DE','N','','Y','D-@P@ @C@','N',22,'DE',1,'de_DE',7),(272,'France','France','FR','N','','Y','@P@ @C@','N',27,'FR',1,'fr_FR',7),(273,'Belgium','Belgium','BE','N','','','B-@P@ @C@','N',16,'BE',1,NULL,7),(274,'Holland','Holland','NL','N','','Y','NL @P@ @C@','N',18,'NL',1,'nl_NL',7),(275,'Spain','Spain','ES','Y','Provincia','Y','E-@P@ @C@','N',24,'ES',1,'es_ES',7),(276,'Switzerland','Switzerland','CH','N','','','CH-@P@ @C@','N',21,'CH',1,'de_CH',7),(277,'Austria','Österreich','AT','N','','','A-@P@ @C@','N',20,'AT',1,'de_AT',7),(278,'Canada','Canada','CA','Y','Province','Y','@C@, @R@ @P@','N',NULL,'',1,'en_US',7),(279,'Afghanistan','Afghanistan','AF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(280,'Albany','Albany','AL','N','','Y','@C@,  @P@','N',NULL,'',1,'sq_AL',7),(281,'Algeria','Algeria','DZ','N','','','@C@,  @P@','N',NULL,'',1,'ar_DZ',7),(282,'American Samoa','American Samoa','AS','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(283,'Andorra','Andorra','AD','N','','','@C@,  @P@','N',24,'AD',1,NULL,7),(284,'Angola','Angola','AO','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(285,'Anguilla','Anguilla','AI','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(286,'Antarctica','Antarctica','AQ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(287,'Antigua And Barbuda','Antigua And Barbuda','AG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(288,'Argentina','Argentina','AR','N','','Y','@C@,  @P@','N',NULL,'',1,'es_AR',7),(289,'Armenia','Armenia','AM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(290,'Aruba','Aruba','AW','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(291,'Australia','Australia','AU','N','','Y','@C@,  @P@','N',NULL,'',1,'en_AU',7),(292,'Azerbaijan','Azerbaijan','AZ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(293,'Bahamas','Bahamas','BS','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(294,'Bahrain','Bahrain','BH','N','','Y','@C@,  @P@','N',NULL,'',1,'ar_BH',7),(295,'Bangladesh','Bangladesh','BD','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(296,'Barbados','Barbados','BB','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(297,'Belarus','Belarus','BY','N','','','@C@,  @P@','N',NULL,'',1,'be_BY',7),(298,'Belize','Belize','BZ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(299,'Benin','Benin','BJ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(300,'Bermuda','Bermuda','BM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(301,'Bhutan','Bhutan','BT','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(302,'Bolivia','Bolivia','BO','N','','','@C@,  @P@','N',NULL,'',1,'es_BO',7),(303,'Bosnia And Herzegovina','Bosnia And Herzegovina','BA','N','','','@C@,  @P@','N',20,'BA',1,NULL,7),(304,'Botswana','Botswana','BW','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(305,'Bouvet Island','Bouvet Island','BV','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(306,'Brazil','Brazil','BR','N','','Y','@C@,  @P@','N',NULL,'',1,'pt_BR',7),(307,'British Indian Ocean Territory','British Indian Ocean Territory','IO','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(308,'Brunei Darussalam','Brunei Darussalam','BN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(309,'Bulgaria','Bulgaria','BG','N','','','@C@,  @P@','N',22,'BG',1,'bg_BG',7),(310,'Burkina Faso','Burkina Faso','BF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(311,'Burundi','Burundi','BI','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(312,'Cambodia','Cambodia','KH','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(313,'Cameroon','Cameroon','CM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(314,'Cape Verde','Cape Verde','CV','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(315,'Cayman Islands','Cayman Islands','KY','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(316,'Central African Republic','Central African Republic','CF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(317,'Chad','Chad','TD','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(318,'Chile','Chile','CL','N','','Y','@C@,  @P@','N',NULL,'',1,'es_CL',7),(319,'China','China','CN','N','','Y','@C@,  @P@','N',NULL,'',1,'zh_CN',7),(320,'Christmas Island','Christmas Island','CX','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(321,'Cocos (Keeling) Islands','Cocos (Keeling) Islands','CC','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(322,'Colombia','Colombia','CO','N','','','@C@,  @P@','N',NULL,'',1,'es_CO',7),(323,'Comoros','Comoros','KM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(324,'Congo','Congo','CG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(325,'Tuvalu','Tuvalu','TV','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(326,'Congo The Democratic Republic Of The','Congo The Democratic Republic Of The','CD','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(327,'Cook Islands','Cook Islands','CK','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(328,'Costa Rica','Costa Rica','CR','N','','','@C@,  @P@','N',NULL,'',1,'es_CR',7),(329,'Cote D Ivoire','Cote D Ivoire','CI','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(330,'Croatia','Croatia','HR','N','','Y','@C@,  @P@','N',21,'HR',1,'hr_HR',7),(331,'Cuba','Cuba','CU','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(332,'Cyprus','Cyprus','CY','N','','','@C@,  @P@','N',28,'CY',1,NULL,7),(333,'Czech Republic','Czech Republic','CZ','N','','','@C@,  @P@','N',24,'CZ',1,'cs_CZ',7),(334,'Denmark','Denmark','DK','N','','Y','@C@,  @P@','N',18,'DK',1,'da_DK',7),(335,'Djibouti','Djibouti','DJ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(336,'Dominica','Dominica','DM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(337,'Dominican Republic','Dominican Republic','DO','N','','','@C@,  @P@','N',NULL,'',1,'es_DO',7),(338,'Ecuador','Ecuador','EC','N','','','@C@,  @P@','N',NULL,'',1,'es_EC',7),(339,'Egypt','Egypt','EG','N','','Y','@C@,  @P@','N',NULL,'',1,'ar_EG',7),(340,'El Salvador','El Salvador','SV','N','','','@C@,  @P@','N',NULL,'',1,'es_SV',7),(341,'Equatorial Guinea','Equatorial Guinea','GQ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(342,'Eritrea','Eritrea','ER','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(343,'Estonia','Estonia','EE','N','','','@C@,  @P@','N',20,'EE',1,'et_EE',7),(344,'Ethiopia','Ethiopia','ET','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(345,'Falkland Islands (Malvinas)','Falkland Islands (Malvinas)','FK','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(346,'Faroe Islands','Faroe Islands','FO','N','','','@C@,  @P@','N',18,'FO',1,NULL,7),(347,'Fiji','Fiji','FJ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(348,'Finland','Finland','FI','N','','Y','@C@,  @P@','N',18,'FI',1,'fi_FI',7),(349,'French Guiana','French Guiana','GF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(350,'French Polynesia','French Polynesia','PF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(351,'French Southern Territories','French Southern Territories','TF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(352,'Gabon','Gabon','GA','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(353,'Gambia','Gambia','GM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(354,'Georgia','Georgia','GE','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(355,'Ghana','Ghana','GH','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(356,'Gibraltar','Gibraltar','GI','N','','','@C@,  @P@','N',23,'GI',1,NULL,7),(357,'Greece','Greece','GR','N','','Y','@C@,  @P@','N',27,'GR',1,'el_GR',7),(358,'Greenland','Greenland','GL','N','','','@C@,  @P@','N',18,'GL',1,NULL,7),(359,'Grenada','Grenada','GD','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(360,'Guadeloupe','Guadeloupe','GP','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(361,'Guam','Guam','GU','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(362,'Guatemala','Guatemala','GT','N','','','@C@,  @P@','N',NULL,'',1,'es_GT',7),(363,'Guinea','Guinea','GN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(364,'Guinea-Bissau','Guinea-Bissau','GW','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(365,'Guyana','Guyana','GY','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(366,'Haiti','Haiti','HT','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(367,'Heard Island And Mcdonald Islands','Heard Island And Mcdonald Islands','HM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(368,'Holy See (Vatican City State)','Holy See (Vatican City State)','VA','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(369,'Honduras','Honduras','HN','N','','','@C@,  @P@','N',NULL,'',1,'es_HN',7),(370,'Hong Kong','Hong Kong','HK','N','','','@C@,  @P@','N',NULL,'',1,'zh_HK',7),(371,'Hungary','Hungary','HU','N','','Y','@C@,  @P@','N',28,'HU',1,'hu_HU',7),(372,'Iceland','Iceland','IS','N','','Y','@C@,  @P@','N',26,'IS',1,'is_IS',7),(373,'India','India','IN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(374,'Indonesia','Indonesia','ID','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(375,'Iran Islamic Republic Of','Iran Islamic Republic Of','IR','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(376,'Iraq','Iraq','IQ','N','','','@C@,  @P@','N',NULL,'',1,'ar_IQ',7),(377,'Ireland','Ireland','IE','N','','Y','@C@,  @P@','N',22,'IE',1,'en_IE',7),(378,'Israel','Israel','IL','N','','','@C@,  @P@','N',23,'IL',1,'iw_IL',7),(379,'Italy','Italy','IT','N','','Y','@C@,  @P@','N',27,'IT',1,'it_IT',7),(380,'Jamaica','Jamaica','JM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(381,'Japan','Japan','JP','N','','Y','@C@,  @P@','N',NULL,'',1,'ja_JP',7),(382,'Jordan','Jordan','JO','N','','','@C@,  @P@','N',NULL,'',1,'ar_JO',7),(383,'Kazakhstan','Kazakhstan','KZ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(384,'Kenya','Kenya','KE','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(385,'Kiribati','Kiribati','KI','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(386,'Korea Democratic People s Republic Of','Korea Democratic People s Republic Of','KP','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(387,'Korea Republic Of','Korea Republic Of','KR','N','','','@C@,  @P@','N',NULL,'',1,'ko_KR',7),(388,'Kuwait','Kuwait','KW','N','','','@C@,  @P@','N',NULL,'',1,'ar_KW',7),(389,'Kyrgyzstan','Kyrgyzstan','KG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(390,'Lao People s Democratic Republic','Lao People s Democratic Republic','LA','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(391,'Latvia','Latvia','LV','N','','','@C@,  @P@','N',21,'LV',1,'lv_LV',7),(392,'Lebanon','Lebanon','LB','N','','','@C@,  @P@','N',NULL,'',1,'ar_LB',7),(393,'Lesotho','Lesotho','LS','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(394,'Liberia','Liberia','LR','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(395,'Libyan Arab Jamahiriya','Libyan Arab Jamahiriya','LY','N','','','@C@,  @P@','N',NULL,'',1,'ar_LY',7),(396,'Liechtenstein','Liechtenstein','LI','N','','','@C@,  @P@','N',21,'LI',1,'de_CH',7),(397,'Lithuania','Lithuania','LT','N','','Y','@C@,  @P@','N',20,'LT',1,'lt_LT',7),(398,'Luxembourg','Luxembourg','LU','N','','Y','L-@P@ @C@','N',20,'LU',1,'fr_LU',7),(399,'Macao','Macao','MO','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(400,'Republic of Macedonia','Republic of Macedonia','MK','N','','','@C@,  @P@','N',19,'MK',1,'mk_MK',7),(401,'Madagascar','Madagascar','MG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(402,'Malawi','Malawi','MW','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(403,'Malaysia','Malaysia','MY','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(404,'Maldives','Maldives','MV','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(405,'Mali','Mali','ML','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(406,'Malta','Malta','MT','N','','','@C@,  @P@','N',31,'MT',1,NULL,7),(407,'Marshall Islands','Marshall Islands','MH','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(408,'Martinique','Martinique','MQ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(409,'Mauritania','Mauritania','MR','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(410,'Mauritius','Mauritius','MU','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(411,'Mayotte','Mayotte','YT','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(412,'Mexico','Mexico','MX','N','','Y','@C@,  @P@','N',NULL,'',1,'es_MX',7),(413,'Micronesia Federated States Of','Micronesia Federated States Of','FM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(414,'Moldova Republic Of','Moldova Republic Of','MD','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(415,'Monaco','Monaco','MC','N','','Y','@C@,  @P@','N',27,'MC',1,'fr_FR',7),(416,'Mongolia','Mongolia','MN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(417,'Montserrat','Montserrat','MS','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(418,'Morocco','Morocco','MA','N','','Y','@C@,  @P@','N',NULL,'',1,'ar_MA',7),(419,'Mozambique','Mozambique','MZ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(420,'Myanmar','Myanmar','MM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(421,'Namibia','Namibia','NA','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(422,'Nauru','Nauru','NR','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(423,'Nepal','Nepal','NP','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(424,'Netherlands Antilles','Netherlands Antilles','AN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(425,'New Caledonia','New Caledonia','NC','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(426,'New Zealand','New Zealand','NZ','N','','Y','@C@,  @P@','N',NULL,'',1,'en_NZ',7),(427,'Nicaragua','Nicaragua','NI','N','','','@C@,  @P@','N',NULL,'',1,'es_NI',7),(428,'Niger','Niger','NE','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(429,'Nigeria','Nigeria','NG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(430,'Niue','Niue','NU','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(431,'Norfolk Island','Norfolk Island','NF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(432,'Northern Mariana Islands','Northern Mariana Islands','MP','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(433,'Norway','Norway','NO','N','','','@C@,  @P@','N',15,'NO',1,'no_NO',7),(434,'Oman','Oman','OM','N','','','@C@,  @P@','N',NULL,'',1,'ar_OM',7),(435,'Pakistan','Pakistan','PK','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(436,'Palau','Palau','PW','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(437,'Palestinian Territory Occupied','Palestinian Territory Occupied','PS','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(438,'Panama','Panama','PA','N','','Y','@C@,  @P@','N',NULL,'',1,'es_PA',7),(439,'Papua New Guinea','Papua New Guinea','PG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(440,'Paraguay','Paraguay','PY','N','','Y','@C@,  @P@','N',NULL,'',1,'es_PY',7),(441,'Peru','Peru','PE','N','','','@C@,  @P@','N',NULL,'',1,'es_PE',7),(442,'Philippines','Philippines','PH','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(443,'Pitcairn','Pitcairn','PN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(444,'Poland','Poland','PL','N','','Y','@C@,  @P@','N',28,'PL',1,'pl_PL',7),(445,'Portugal','Portugal','PT','N','','','@C@,  @P@','N',25,'PT',1,'pt_PT',7),(446,'Puerto Rico','Puerto Rico','PR','N','','','@C@,  @P@','N',NULL,'',1,'es_PR',7),(447,'Qatar','Qatar','QA','N','','Y','@C@,  @P@','N',NULL,'',1,'ar_QA',7),(448,'Reunion','Reunion','RE','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(449,'Romania','Romania','RO','N','','Y','@C@,  @P@','N',24,'RO',1,'ro_RO',7),(450,'Russian Federation','Russian Federation','RU','N','','','@C@,  @P@','N',NULL,'',1,'ru_RU',7),(451,'Rwanda','Rwanda','RW','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(452,'Saint Helena','Saint Helena','SH','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(453,'Saint Kitts And Nevis','Saint Kitts And Nevis','KN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(454,'Saint Lucia','Saint Lucia','LC','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(455,'Saint Pierre And Miquelon','Saint Pierre And Miquelon','PM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(456,'Saint Vincent And The Grenadines','Saint Vincent And The Grenadines','VC','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(457,'Samoa','Samoa','WS','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(458,'San Marino','San Marino','SM','N','','','@C@,  @P@','N',27,'SM',1,NULL,7),(459,'Sao Tome And Principe','Sao Tome And Principe','ST','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(460,'Saudi Arabia','Saudi Arabia','SA','N','','Y','@C@,  @P@','N',NULL,'',1,'ar_SA',7),(461,'Senegal','Senegal','SN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(462,'Seychelles','Seychelles','SC','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(463,'Sierra Leone','Sierra Leone','SL','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(464,'Singapore','Singapore','SG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(465,'Slovakia','Slovakia','SK','N','','Y','@C@,  @P@','N',24,'SK',1,'sk_SK',7),(466,'Slovenia','Slovenia','SI','N','','','@C@,  @P@','N',19,'SI',1,'sl_SI',7),(467,'Solomon Islands','Solomon Islands','SB','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(468,'Somalia','Somalia','SO','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(469,'South Africa','South Africa','ZA','N','','Y','@C@,  @P@','N',NULL,'',1,'en_ZA',7),(470,'South Georgia And The South Sandwich Islands','South Georgia And The South Sandwich Islands','GS','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(471,'Sri Lanka','Sri Lanka','LK','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(472,'Sudan','Sudan','SD','N','','','@C@,  @P@','N',NULL,'',1,'ar_SD',7),(473,'Suriname','Suriname','SR','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(474,'Svalbard And Jan Mayen','Svalbard And Jan Mayen','SJ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(475,'Swaziland','Swaziland','SZ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(476,'Sweden','Sweden','SE','N','','Y','@C@,  @P@','N',24,'SE',1,'sv_SE',7),(477,'Syrian Arab Republic','Syrian Arab Republic','SY','N','','','@C@,  @P@','N',NULL,'',1,'ar_SY',7),(478,'Taiwan','Taiwan','TW','N','','','@C@,  @P@','N',NULL,'',1,'zh_TW',7),(479,'Tajikistan','Tajikistan','TJ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(480,'Tanzania United Republic Of','Tanzania United Republic Of','TZ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(481,'Thailand','Thailand','TH','N','','Y','@C@,  @P@','N',NULL,'',1,'th_TH',7),(482,'Timor-Leste','Timor-Leste','TL','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(483,'Togo','Togo','TG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(484,'Tokelau','Tokelau','TK','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(485,'Tonga','Tonga','TO','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(486,'Trinidad And Tobago','Trinidad And Tobago','TT','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(487,'Tunisia','Tunisia','TN','N','','Y','@C@,  @P@','N',24,'TN',1,'ar_TN',7),(488,'Turkey','Turkey','TR','N','','Y','@C@,  @P@','N',26,'TR',1,'tr_TR',7),(489,'Turkmenistan','Turkmenistan','TM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(490,'Turks And Caicos Islands','Turks And Caicos Islands','TC','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(491,'Uganda','Uganda','UG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(492,'Ukraine','Ukraine','UA','N','','','@C@,  @P@','N',NULL,'',1,'uk_UA',7),(493,'United Arab Emirates','United Arab Emirates','AE','N','','','@C@,  @P@','N',NULL,'',1,'ar_AE',7),(494,'United Kingdom','United Kingdom','GB','N','','Y','@C@,  @P@','N',22,'GB',1,'en_GB',7),(495,'United States Minor Outlying Islands','United States Minor Outlying Islands','UM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(496,'Uruguay','Uruguay','UY','N','','Y','@C@,  @P@','N',NULL,'',1,'es_UY',7),(497,'Uzbekistan','Uzbekistan','UZ','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(498,'Vanuatu','Vanuatu','VU','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(499,'Venezuela','Venezuela','VE','N','','','@C@,  @P@','N',NULL,'',1,'es_VE',7),(500,'Viet Nam','Viet Nam','VN','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(501,'Virgin Islands British','Virgin Islands British','VG','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(502,'Virgin Islands U.S.','Virgin Islands U.S.','VI','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(503,'Wallis And Futuna','Wallis And Futuna','WF','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(504,'Western Sahara','Western Sahara','EH','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(505,'Yemen','Yemen','YE','N','','','@C@,  @P@','N',NULL,'',1,'ar_YE',7),(506,'Zambia','Zambia','ZM','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(507,'Zimbabwe','Zimbabwe','ZW','N','','','@C@,  @P@','N',NULL,'',1,NULL,7),(508,'Republic of Serbia','Republic of Serbia','RS','N','State','','@C@,  @P@','N',22,'RS',1,'sr_RS',7),(509,'Montenegro','Montenegro','ME','N','State','','@C@,  @P@','N',22,'ME',1,NULL,7);
/*!40000 ALTER TABLE `cb_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cb_currency`
--

DROP TABLE IF EXISTS `cb_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_currency` (
  `idcurrency` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL,
  `isactive` varchar(1) NOT NULL DEFAULT 'N',
  `isocode` varchar(3) NOT NULL,
  `cursymbol` varchar(10) DEFAULT NULL,
  `precisionstd` decimal(10,0) NOT NULL,
  `precisioncost` decimal(10,0) NOT NULL,
  `precisionprize` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idcurrency`),
  UNIQUE KEY `u_cb_currency_currency` (`currency`),
  UNIQUE KEY `u_cb_currency_isocode` (`isocode`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=latin1 COMMENT='Tabla  donde se definen las monedas disponibles y sus relaciones a partir de las monedas bases.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_currency`
--

LOCK TABLES `cb_currency` WRITE;
/*!40000 ALTER TABLE `cb_currency` DISABLE KEYS */;
INSERT INTO `cb_currency` VALUES (6,'US Dollar','US Dollar','Y','USD','$',2,4,4),(7,'Euro','Euro','Y','EUR','E',2,4,4),(8,'Deutsche Mark','Deutsche Mark','N','DEM','DM',2,4,4),(9,'Austrian Schilling','Austrian Schilling','N','ATS','Sch',2,4,4),(10,'Belgian Franc','Belgian Franc','N','BEF','BFr',0,0,0),(11,'Finish Mark','Finish Mark','N','FIM','FM',2,4,4),(12,'Irish Pound','Irish Pound','N','IEP','I£',2,4,4),(13,'Italian Lira','Italian Lira','N','ITL','L',0,0,0),(14,'Dutch Guilder','Dutch Guilder','N','NLF','Fl',2,4,4),(15,'Portugese Escudo','Portugese Escudo','N','PTE','Es',0,0,0),(16,'Spannish Peseto','Spannish Peseto','N','ESP','Pts',0,0,0),(17,'French Franc','French Franc','N','FRF','Fr',2,4,4),(18,'Japanese Yen','Japanese Yen','Y','JPY','¥',0,0,0),(19,'British Pound','British Pound','Y','GBP','£',2,4,4),(20,'Swiss Franc SFR','Swiss Franc','Y','SFR','SFr',2,4,4),(21,'Canadian Dollar','Canadian Dollar','Y','CAD','C$',2,4,4),(22,'Argentine Peso','Argentine Peso','Y','ARS','$',2,4,4),(23,'Australian Dollar','Australian Dollar','Y','AUD','$',2,4,4),(24,'New Zealand Dollar','New Zealand Dollar','Y','NZD','$',2,4,4),(25,'Liberian Dollar','Liberian Dollar','Y','LRD','$',2,4,4),(26,'Mexican Peso','Mexican Peso','Y','MXN','$',2,4,4),(27,'Peso Uruguayo','Peso Uruguayo','Y','UYU','$U',2,4,4),(28,'Armenian Dram','Armenian Dram','Y','AMD','',2,4,4),(29,'Kwanza','Kwanza','Y','AOA','',2,4,4),(30,'Azerbaijanian Manat','Azerbaijanian Manat','Y','AZM','',2,4,4),(31,'Boliviano','Boliviano','Y','BOB','',2,4,4),(32,'Franc Congolais','Franc Congolais','Y','CDF','',2,4,4),(33,'Czech Koruna','Czech Koruna','Y','CZK','',2,4,4),(34,'Lari','Lari','Y','GEL','',2,4,4),(35,'Iranian Rial','Iranian Rial','Y','IRR','',2,4,4),(36,'Lithuanian Litus','Lithuanian Litus','Y','LTL','',2,4,4),(37,'Moldovan Leu','Moldovan Leu','Y','MDL','',2,4,4),(38,'Philippine Peso','Philippine Peso','Y','PHP','',2,4,4),(39,'Zloty','Zloty','Y','PLN','',2,4,4),(40,'Russian Ruble','Russian Ruble','Y','RUR','',2,4,4),(41,'Sudanese Dinar','Sudanese Dinar','Y','SDD','',2,4,4),(42,'Somoni','Somoni','Y','TJS','',2,4,4),(43,'Manat','Manat','Y','TMM','',2,4,4),(44,'Timor Escudo','Timor Escudo','Y','TPE','',0,2,2),(45,'Hryvnia','Hryvnia','Y','UAH','',2,4,4),(46,'Uzbekistan Sum','Uzbekistan Sum','Y','UZS','',2,4,4),(47,'Cyprus Pound','Cyprus Pound','Y','CYP','£C',2,4,4),(48,'Egyptian Pound','Egyptian Pound','Y','EGP','£E',2,4,4),(49,'Falkland Islands Pound','Falkland Islands Pound','Y','FKP','£F',2,4,4),(50,'Gibraltar Pound','Gibraltar Pound','Y','GIP','£G',2,4,4),(51,'Saint Helena Pound','Saint Helena Pound','Y','SHP','£S',2,4,4),(52,'Syrian Pound','Syrian Pound','Y','SYP','£S',2,4,4),(53,'Cedi','Cedi','Y','GHC','¢',2,4,4),(54,'El Salvador Colon','El Salvador Colon','Y','SVC','¢',2,4,4),(55,'Afghani','Afghani','Y','AFA','Af',2,4,4),(56,'Aruban Guilder','Aruban Guilder','Y','AWG','Af.',2,4,4),(57,'Balboa','Balboa','Y','PAB','B',2,4,4),(58,'Brunei Dollar','Brunei Dollar','Y','BND','B$',2,4,4),(59,'Bahamian Dollar','Bahamian Dollar','Y','BSD','B$',2,4,4),(60,'Bahraini Dinar','Bahraini Dinar','Y','BHD','BD',3,5,5),(61,'Bermudian Dollar','Bermudian Dollar','Y','BMD','Bd$',2,4,4),(62,'Barbados Dollar','Barbados Dollar','Y','BBD','Bds$',2,4,4),(63,'Belarussian Ruble','Belarussian Ruble','Y','BYR','BR',0,2,2),(64,'Ethiopian Birr','Ethiopian Birr','Y','ETB','Br',2,4,4),(65,'Bolivar','Bolivar','Y','VEB','Bs',2,4,4),(66,'Baht','Baht','Y','THB','Bt',2,4,4),(67,'Belize Dollar','Belize Dollar','Y','BZD','BZ$',2,4,4),(68,'Cordoba Oro','Cordoba Oro','Y','NIO','C$',2,4,4),(69,'Cape Verde Escudo','Cape Verde Escudo','Y','CVE','C.V.Esc.',2,4,4),(70,'Comoro Franc','Comoro Franc','Y','KMF','CF',0,2,2),(71,'CFA Franc BCEAO','CFA Franc BCEAO','Y','XOF','CFAF',0,2,2),(72,'CFA Franc BEAC','CFA Franc BEAC','Y','XAF','CFAF',0,2,2),(73,'CFP Franc','CFP Franc','Y','XPF','CFPF',0,2,2),(74,'Chilean Peso','Chilean Peso','Y','CLP','Ch$',0,2,2),(75,'Cayman Islands Dollar','Cayman Islands Dollar','Y','KYD','CI$',2,4,4),(76,'Colombian Peso','Colombian Peso','Y','COP','Col$',2,4,4),(77,'Riel','Riel','Y','KHR','CR',2,4,4),(78,'Cuban Peso','Cuban Peso','Y','CUP','Cu$',2,4,4),(79,'Dalasi','Dalasi','Y','GMD','D',2,4,4),(80,'Dong','Dong','Y','VND','D',2,4,4),(81,'Algerian Dinar','Algerian Dinar','Y','DZD','DA',2,4,4),(82,'Dobra','Dobra','Y','STD','Db',2,4,4),(83,'Djibouti Franc','Djibouti Franc','Y','DJF','DF',0,2,2),(84,'UAE Dirham','UAE Dirham','Y','AED','Dh',2,4,4),(85,'Moroccan Dirham','Moroccan Dirham','Y','MAD','DH',2,4,4),(86,'Yugoslavian Dinar','Yugoslavian Dinar','Y','YUM','Din',2,4,4),(87,'Danish Krone','Danish Krone','Y','DKK','Dkr',2,4,4),(88,'East Caribbean Dollar','East Caribbean Dollar','Y','XCD','EC$',2,4,4),(89,'Fiji Dollar','Fiji Dollar','Y','FJD','F$',2,4,4),(90,'Burundi Franc','Burundi Franc','Y','BIF','FBu',0,2,2),(91,'Malagasy Franc','Malagasy Franc','Y','MGF','FMG',0,2,2),(92,'Forint','Forint','Y','HUF','Ft',2,4,4),(93,'Gourde','Gourde','Y','HTG','G',2,4,4),(94,'Guyana Dollar','Guyana Dollar','Y','GYD','G$',2,4,4),(95,'Hong Kong Dollar','Hong Kong Dollar','Y','HKD','HK$',2,4,4),(96,'Croatian Kuna','Croatian Kuna','Y','HRK','HRK',2,4,4),(97,'Iraqi Dinar','Iraqi Dinar','Y','IQD','ID',3,5,5),(98,'Iceland Krona','Iceland Krona','Y','ISK','IKr',2,4,4),(99,'Jamaican Dollar','Jamaican Dollar','Y','JMD','J$',2,4,4),(100,'Jordanian Dinar','Jordanian Dinar','Y','JOD','JD',3,5,5),(101,'Kyat','Kyat','Y','MMK','K',2,4,4),(102,'Kina','Kina','Y','PGK','K',2,4,4),(103,'Kenyan Shilling','Kenyan Shilling','Y','KES','K Sh',2,4,4),(104,'Kuwaiti Dinar','Kuwaiti Dinar','Y','KWD','KD',3,5,5),(105,'Convertible Marks','Convertible Marks','Y','BAM','KM',2,4,4),(106,'Kip','Kip','Y','LAK','KN',2,4,4),(107,'Nakfa','Nakfa','Y','ERN','KR',2,4,4),(108,'Lek','Lek','Y','ALL','L',2,4,4),(109,'Lempira','Lempira','Y','HNL','L',2,4,4),(110,'Leu','Leu','Y','ROL','L',2,4,4),(111,'Lilangeni','Lilangeni','Y','SZL','L',2,4,4),(112,'Libyan Dinar','Libyan Dinar','Y','LYD','LD',3,5,5),(113,'Leone','Leone','Y','SLL','Le',2,4,4),(114,'Maltese Lira','Maltese Lira','Y','MTL','Lm',2,4,4),(115,'Latvian Lats','Latvian Lats','Y','LVL','Ls',2,4,4),(116,'Lev','Lev','Y','BGL','Lv',2,4,4),(117,'Mauritius Rupee','Mauritius Rupee','Y','MUR','Mau Rs',2,4,4),(118,'Kwacha MWK','Kwacha','Y','MWK','MK',2,4,4),(119,'Denar','Denar','Y','MKD','MKD',2,4,4),(120,'Metical','Metical','Y','MZM','Mt',2,4,4),(121,'Netherlands Antillian Guilder','Netherlands Antillian Guilder','Y','ANG','NAf.',2,4,4),(122,'Kroon','Kroon','Y','EEK','Nfa',2,4,4),(123,'New Israeli Sheqel','New Israeli Sheqel','Y','ILS','NIS',2,4,4),(124,'Norwegian Krone','Norwegian Krone','Y','NOK','NKr',2,4,4),(125,'Nepalese Rupee','Nepalese Rupee','Y','NPR','NRs',2,4,4),(126,'New Taiwan Dollar','New Taiwan Dollar','Y','TWD','NT$',2,4,4),(127,'Pula','Pula','Y','BWP','P',2,4,4),(128,'Pataca','Pataca','Y','MOP','P',2,4,4),(129,'Quetzal','Quetzal','Y','GTQ','Q',2,4,4),(130,'Qatari Rial','Qatari Rial','Y','QAR','QR',2,4,4),(131,'Rand','Rand','Y','ZAR','R',2,4,4),(132,'Brazilian Real','Brazilian Real','Y','BRL','R$',2,4,4),(133,'Dominican Peso','Dominican Peso','Y','DOP','RD$',2,4,4),(134,'Rufiyaa','Rufiyaa','Y','MVR','Rf',2,4,4),(135,'Rwanda Franc','Rwanda Franc','Y','RWF','RF',0,2,2),(136,'Malaysian Ringgit','Malaysian Ringgit','Y','MYR','RM',2,4,4),(137,'Rial Omani','Rial Omani','Y','OMR','RO',3,5,5),(138,'Rupiah','Rupiah','Y','IDR','Rp',2,4,4),(139,'Indian Rupee','Indian Rupee','Y','INR','Rs',2,4,4),(140,'Pakistan Rupee','Pakistan Rupee','Y','PKR','Rs',2,4,4),(141,'Singapore Dollar','Singapore Dollar','Y','SGD','S$',2,4,4),(142,'Nuevo Sol','Nuevo Sol','Y','PEN','S/.',2,4,4),(143,'Suriname Guilder','Suriname Guilder','Y','SRG','Sf.',2,4,4),(144,'Solomon Islands Dollar','Solomon Islands Dollar','Y','SBD','SI$',2,4,4),(145,'Swedish Krona','Swedish Krona','Y','SEK','Sk',2,4,4),(146,'Slovak Koruna','Slovak Koruna','Y','SKK','Sk',2,4,4),(147,'Sri Lanka Rupee','Sri Lanka Rupee','Y','LKR','SLRs',2,4,4),(148,'Somali Shilling','Somali Shilling','Y','SOS','So. Sh.',2,4,4),(149,'Seychelles Rupee','Seychelles Rupee','Y','SCR','SR',2,4,4),(150,'Saudi Riyal','Saudi Riyal','Y','SAR','SRls',2,4,4),(151,'Swiss Franc CHF','Swiss Franc','Y','CHF','SwF',2,4,4),(152,'Pa’anga','Pa’anga','Y','TOP','T$',2,4,4),(153,'Tunisian Dinar','Tunisian Dinar','Y','TND','TD',3,5,5),(154,'Taka','Taka','Y','BDT','Tk',2,4,4),(155,'Turkish Lira','Turkish Lira','Y','TRL','TL',0,2,2),(156,'Tanzanian Shilling','Tanzanian Shilling','Y','TZS','TSh',2,4,4),(157,'Trinidad and Tobago Dollar','Trinidad and Tobago Dollar','Y','TTD','TT$',2,4,4),(158,'Tugrik','Tugrik','Y','MNT','Tug',2,4,4),(159,'Ouguiya','Ouguiya','Y','MRO','UM',2,4,4),(160,'Uganda Shilling','Uganda Shilling','Y','UGX','USh',2,4,4),(161,'Vatu','Vatu','Y','VUV','VT',0,2,2),(162,'Won','Won','Y','KRW','W',0,2,2),(163,'Tala','Tala','Y','WST','WS$',2,4,4),(164,'Yuan Renminbi','Yuan Renminbi','Y','CNY','Y',2,4,4),(165,'Yemeni Rial','Yemeni Rial','Y','YER','YRls',2,4,4),(166,'Zimbabwe Dollar','Zimbabwe Dollar','Y','ZWD','Z$',2,4,4),(167,'Kwacha ZMK','Kwacha','Y','ZMK','ZK',2,4,4),(168,'Costa Rican Colon','Costa Rican Colon','Y','CRC','',2,4,4),(169,'Guinea Franc','Guinea Franc','Y','GNF','',0,2,2),(170,'Guinea-Bissau Peso','Guinea-Bissau Peso','Y','GWP','',2,4,4),(171,'Som','Som','Y','KGS','',2,4,4),(172,'North Korean Won','North Korean Won','Y','KPW','',2,4,4),(173,'Tenge','Tenge','Y','KZT','',2,4,4),(174,'Lebanese Pound','Lebanese Pound','Y','LBP','',2,4,4),(175,'Naira','Naira','Y','NGN','',2,4,4),(176,'Guarani','Guarani','Y','PYG','',0,2,2),(177,'Serbian Dinar','Serbian Dinar','Y','RSD','',2,4,4);
/*!40000 ALTER TABLE `cb_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cb_customer`
--

DROP TABLE IF EXISTS `cb_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_customer` (
  `idcustomer` int(11) NOT NULL AUTO_INCREMENT,
  `identerprise` int(11) DEFAULT NULL,
  `customer` varchar(15) NOT NULL,
  `customername` varchar(150) DEFAULT NULL,
  `customeralias` varchar(100) DEFAULT NULL,
  `contact` varchar(250) DEFAULT NULL,
  `customerstate` varchar(30) DEFAULT NULL,
  `sale` decimal(10,3) DEFAULT NULL,
  `identitynumber` varchar(100) DEFAULT NULL,
  `customerpayer` varchar(20) DEFAULT NULL,
  `idpaymentmethod` int(11) DEFAULT NULL,
  `idcountry` int(11) DEFAULT NULL,
  `idcurrency` int(11) DEFAULT NULL,
  `idlanguage` varchar(6) DEFAULT NULL,
  `idaddresses` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcustomer`),
  UNIQUE KEY `un_cb_customer_cb_enterprise` (`identerprise`,`customer`),
  KEY `fk_cb_customer_idaddresses` (`idaddresses`),
  KEY `fk_cb_customer_idcountry` (`idcountry`),
  KEY `fk_cb_customer_idcurrency` (`idcurrency`),
  KEY `fk_cb_customer_idlanguage` (`idlanguage`),
  KEY `fk_cb_customer_idpaymentmethod` (`idpaymentmethod`),
  CONSTRAINT `fk_cb_customer_idaddresses` FOREIGN KEY (`idaddresses`) REFERENCES `cb_addresses` (`idaddresses`),
  CONSTRAINT `fk_cb_customer_idcountry` FOREIGN KEY (`idcountry`) REFERENCES `cb_country` (`idcountry`),
  CONSTRAINT `fk_cb_customer_idcurrency` FOREIGN KEY (`idcurrency`) REFERENCES `cb_currency` (`idcurrency`),
  CONSTRAINT `fk_cb_customer_identerprise` FOREIGN KEY (`identerprise`) REFERENCES `cb_enterprise` (`identerprise`),
  CONSTRAINT `fk_cb_customer_idlanguage` FOREIGN KEY (`idlanguage`) REFERENCES `cb_language` (`idlanguage`),
  CONSTRAINT `fk_cb_customer_idpaymentmethod` FOREIGN KEY (`idpaymentmethod`) REFERENCES `cb_paymentmethod` (`idpaymentmethod`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabla donde se almacenarán los clientes de las diferentes empresas, se entiende cliente como aquel que compra a una empresa.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_customer`
--

LOCK TABLES `cb_customer` WRITE;
/*!40000 ALTER TABLE `cb_customer` DISABLE KEYS */;
INSERT INTO `cb_customer` VALUES (2,100,'CU800900','Xules Code','Xules','Xules','ACTIVE',100.000,'47392837W','Xules',2,275,7,'es_ES',5);
/*!40000 ALTER TABLE `cb_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cb_enterprise`
--

DROP TABLE IF EXISTS `cb_enterprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_enterprise` (
  `identerprise` int(11) NOT NULL,
  `enterprise` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `enterprisealias` varchar(100) DEFAULT NULL,
  `contact` varchar(250) DEFAULT NULL,
  `estate` varchar(30) DEFAULT NULL,
  `balance` decimal(10,3) DEFAULT NULL,
  `ei` varchar(100) DEFAULT NULL,
  `enterprisepayer` varchar(20) DEFAULT NULL,
  `idcountry` int(11) DEFAULT NULL,
  `idcurrency` int(11) DEFAULT NULL,
  `idlanguage` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`identerprise`),
  KEY `cb_enterprise_idlanguage` (`idlanguage`),
  KEY `fk_cb_enterprise_idcountry` (`idcountry`),
  KEY `fk_cb_enterprise_idcurrency` (`idcurrency`),
  CONSTRAINT `cb_enterprise_idlanguage` FOREIGN KEY (`idlanguage`) REFERENCES `cb_language` (`idlanguage`),
  CONSTRAINT `fk_cb_enterprise_idcountry` FOREIGN KEY (`idcountry`) REFERENCES `cb_country` (`idcountry`),
  CONSTRAINT `fk_cb_enterprise_idcurrency` FOREIGN KEY (`idcurrency`) REFERENCES `cb_currency` (`idcurrency`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla para controlar las empresas que se usan en la aplicación, la aplicación se desarrolla en función de estos parámetros ya que habrá algunas tablas que serán comunes a las empresa y otras que no, por ejemplo, cada empresa tendrá sus propios productos, pero tendrá los mismos tipos de documentos de trabajo.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_enterprise`
--

LOCK TABLES `cb_enterprise` WRITE;
/*!40000 ALTER TABLE `cb_enterprise` DISABLE KEYS */;
INSERT INTO `cb_enterprise` VALUES (100,'LEARNING PROJECT','Learning project enterprise','LP','Xules','ACTIVE',10.000,'78678998R','Xules',275,7,'es_ES');
/*!40000 ALTER TABLE `cb_enterprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cb_language`
--

DROP TABLE IF EXISTS `cb_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_language` (
  `idlanguage` varchar(6) NOT NULL COMMENT 'Como clave primaria usamos la codificación del idioma i18n e i10n, las principales: es_ES y en_EN, que serán las que se usarán por defecto.',
  `namelanguage` varchar(60) NOT NULL COMMENT 'Nombre del idioma en el idioma por defecto del sistema (castellano).',
  `isactive` varchar(1) NOT NULL DEFAULT 'N',
  `languageiso` varchar(2) DEFAULT NULL,
  `countrycode` varchar(2) DEFAULT NULL,
  `isbaselanguage` varchar(1) NOT NULL DEFAULT 'N',
  `issystemlanguage` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`idlanguage`),
  UNIQUE KEY `u_cb_language_namelanguage` (`namelanguage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Como clave primaria usamos la codificación del idioma i18n e i10n, las principales: es_ES y en_EN, que serán las que se usarán por defecto.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_language`
--

LOCK TABLES `cb_language` WRITE;
/*!40000 ALTER TABLE `cb_language` DISABLE KEYS */;
INSERT INTO `cb_language` VALUES ('ar_AE','Arabic (United Arab Emirates)','Y','ar','AE','N','N'),('ar_BH','Arabic (Bahrain)','Y','ar','BH','N','N'),('ar_DZ','Arabic (Algeria)','Y','ar','DZ','N','N'),('ar_EG','Arabic (Egypt)','Y','ar','EG','N','N'),('ar_IQ','Arabic (Iraq)','Y','ar','IQ','N','N'),('ar_JO','Arabic (Jordan)','Y','ar','JO','N','N'),('ar_KW','Arabic (Kuwait)','Y','ar','KW','N','N'),('ar_LB','Arabic (Lebanon)','Y','ar','LB','N','N'),('ar_LY','Arabic (Libya)','Y','ar','LY','N','N'),('ar_MA','Arabic (Morocco)','Y','ar','MA','N','N'),('ar_OM','Arabic (Oman)','Y','ar','OM','N','N'),('ar_QA','Arabic (Qatar)','Y','ar','QA','N','N'),('ar_SA','Arabic (Saudi Arabia)','Y','ar','SA','N','N'),('ar_SD','Arabic (Sudan)','Y','ar','SD','N','N'),('ar_SY','Arabic (Syria)','Y','ar','SY','N','N'),('ar_TN','Arabic (Tunisia)','Y','ar','TN','N','N'),('ar_YE','Arabic (Yemen)','Y','ar','YE','N','N'),('be_BY','Byelorussian (Belarus)','Y','be','BY','N','N'),('bg_BG','Bulgarian (Bulgaria)','Y','bg','BG','N','N'),('ca_ES','Catalan (Spain)','Y','ca','ES','N','N'),('cs_CZ','Czech (Czech Republic)','Y','cs','CZ','N','N'),('da_DK','Danish (Denmark)','Y','da','DK','N','N'),('de_AT','German (Austria)','Y','de','AT','N','N'),('de_CH','German (Switzerland)','Y','de','CH','N','N'),('de_DE','German (Germany)','Y','de','DE','N','N'),('de_LU','German (Luxembourg)','Y','de','LU','N','N'),('el_GR','Greek (Greece)','Y','el','GR','N','N'),('en_AU','English (Australia)','Y','en','AU','N','N'),('en_CA','English (Canada)','Y','en','CA','N','N'),('en_GB','English (United Kingdom)','Y','en','GB','N','N'),('en_IE','English (Ireland)','Y','en','IE','N','N'),('en_IN','English (India)','Y','en','IN','N','N'),('en_NZ','English (New Zealand)','Y','en','NZ','N','N'),('en_US','English (USA)','Y','en','US','N','N'),('en_ZA','English (South Africa)','Y','en','ZA','N','N'),('es_AR','Spanish (Argentina)','Y','es','AR','N','N'),('es_BO','Spanish (Bolivia)','Y','es','BO','N','N'),('es_CL','Spanish (Chile)','Y','es','CL','N','N'),('es_CO','Spanish (Colombia)','Y','es','CO','N','N'),('es_CR','Spanish (Costa Rica)','Y','es','CR','N','N'),('es_DO','Spanish (Dominican Republic)','Y','es','DO','N','N'),('es_EC','Spanish (Ecuador)','Y','es','EC','N','N'),('es_ES','Spanish (Spain)','Y','es','ES','Y','N'),('es_GT','Spanish (Guatemala)','Y','es','GT','N','N'),('es_HN','Spanish (Honduras)','Y','es','HN','N','N'),('es_MX','Spanish (Mexico)','Y','es','MX','N','N'),('es_NI','Spanish (Nicaragua)','Y','es','NI','N','N'),('es_PA','Spanish (Panama)','Y','es','PA','N','N'),('es_PE','Spanish (Peru)','Y','es','PE','N','N'),('es_PR','Spanish (Puerto Rico)','Y','es','PR','N','N'),('es_PY','Spanish (Paraguay)','Y','es','PY','N','N'),('es_SV','Spanish (El Salvador)','Y','es','SV','N','N'),('es_UY','Spanish (Uruguay)','Y','es','UY','N','N'),('es_VE','Spanish (Venezuela)','Y','es','VE','N','N'),('et_EE','Estonian (Estonia)','Y','et','EE','N','N'),('eu_ES','Basque (Spain)','Y','eu','ES','N','N'),('fa_IR','Farsi (Iran)','Y','fa','IR','N','N'),('fi_FI','Finnish (Finland)','Y','fi','FI','N','N'),('fr_BE','French (Belgium)','Y','fr','BE','N','N'),('fr_CA','French (Canada)','Y','fr','CA','N','N'),('fr_CH','French (Switzerland)','Y','fr','CH','N','N'),('fr_FR','French (France)','Y','fr','FR','N','N'),('fr_LU','French (Luxembourg)','Y','fr','LU','N','N'),('gl_ES','Galician','Y','  ','  ','N','N'),('hi_IN','Hindi (India)','Y','hi','IN','N','N'),('hr_HR','Croatian (Croatia)','Y','hr','HR','N','N'),('hu_HU','Hungarian (Hungary)','Y','hu','HU','N','N'),('is_IS','Icelandic (Iceland)','Y','is','IS','N','N'),('it_CH','Italian (Switzerland)','Y','it','CH','N','N'),('it_IT','Italian (Italy)','Y','it','IT','N','N'),('iw_IL','Hebrew (Israel)','Y','iw','IL','N','N'),('ja_JP','Japanese (Japan)','Y','ja','JP','N','N'),('ko_KR','Korean (South Korea)','Y','ko','KR','N','N'),('lt_LT','Lithuanian (Lithuania)','Y','lt','LT','N','N'),('lv_LV','Latvian (Lettish) (Latvia)','Y','lv','LV','N','N'),('mk_MK','Macedonian (Macedonia)','Y','mk','MK','N','N'),('nl_BE','Dutch (Belgium)','Y','nl','BE','N','N'),('nl_NL','Dutch (Netherlands)','Y','nl','NL','N','N'),('no_NO','Norwegian (Norway)','Y','no','NO','N','N'),('pl_PL','Polish (Poland)','Y','pl','PL','N','N'),('pt_BR','Portuguese (Brazil)','Y','pt','BR','N','N'),('pt_PT','Portuguese (Portugal)','Y','pt','PT','N','N'),('ro_RO','Romanian (Romania)','Y','ro','RO','N','N'),('ru_RU','Russian (Russia)','Y','ru','RU','N','N'),('sh_YU','Serbo-Croatian (Yugoslavia)','Y','sh','YU','N','N'),('sk_SK','Slovak (Slovakia)','Y','sk','SK','N','N'),('sl_SI','Slovenian (Slovenia)','Y','sl','SI','N','N'),('sq_AL','Albanian (Albania)','Y','sq','AL','N','N'),('sr_RS','Serbian (Republic of Serbia)','Y','sr','RS','N','N'),('sv_SE','Swedish (Sweden)','Y','sv','SE','N','N'),('th_TH','Thai (Thailand)','Y','th','TH','N','N'),('tr_TR','Turkish (Turkey)','Y','tr','TR','N','N'),('uk_UA','Ukrainian (Ukraine)','Y','uk','UA','N','N'),('vi_VN','Vietnamese','Y','  ','  ','N','N'),('zh_CN','Chinese (China)','Y','zh','CN','N','N'),('zh_HK','Chinese (Hong Kong)','Y','zh','HK','N','N'),('zh_TW','Chinese (Taiwan)','Y','zh','TW','N','N');
/*!40000 ALTER TABLE `cb_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cb_paymentmethod`
--

DROP TABLE IF EXISTS `cb_paymentmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cb_paymentmethod` (
  `idpaymentmethod` int(11) NOT NULL AUTO_INCREMENT,
  `paymentmethod` varchar(100) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `paymentterms` varchar(250) DEFAULT NULL,
  `paymententity` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idpaymentmethod`),
  UNIQUE KEY `un_cb_paymentmethod_paymentmethod` (`paymentmethod`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cb_paymentmethod`
--

LOCK TABLES `cb_paymentmethod` WRITE;
/*!40000 ALTER TABLE `cb_paymentmethod` DISABLE KEYS */;
INSERT INTO `cb_paymentmethod` VALUES (2,'BANCO','PAGO BANCARIO','Transferencia bancaria directa.','BANCO'),(3,'TRANSFERENCIA','Transferencia bancaria','Un solo pago por transferencia','BANCO'),(4,'CHEQUE ','Cheque al portador 30 días','Cheque al portador 30 días','BANCO');
/*!40000 ALTER TABLE `cb_paymentmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `remember_token` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Juan','j.irabuena@gmail.com','$2y$10$Y6gI.Tfpz49PVXLGXzpQeOEzUbj1h5qXmt9yVB4aaPPYvhlHsucwS','2017-10-13','2017-10-13','NgUBKQNllgHEXMY6uysADZ5dNX6P8Ze2J49GTwVo0aV0td03BhJZp4zHlBoh'),(3,'Juanma','juan@towerhousestudio.com','$2y$10$32MPAdULFjDBxCWxG4KUMePIYoQ.fg54OZxcVtMyh8kPf3XN4Zxx2','2017-10-13','2017-10-13','rFuCdwTaOEYx4WvsZxysQSgeuMpKEoYMj2RTxYnLrArPbpGkF7UzXCWpso9W'),(4,'Rafael','rafael.falkenstein@towerhousestudio.com','$2y$10$xGqNDZhmZudOR.28BA3V1eVlamJlZ.yoxhUSJeFlhcuD7FovRrQcK','2017-10-13','2017-10-13',NULL),(5,'fran','asd@g.com','$2y$10$x69Uq6Q5mNQCa2nwvr1as.AAiUwxRl.AxwSwEuY5RYCRKDPt3T2p2','2017-10-13','2017-10-13',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'MariaDB'
--

--
-- Dumping routines for database 'MariaDB'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-13 18:26:55
